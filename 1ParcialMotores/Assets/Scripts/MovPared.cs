using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MovPared : MonoBehaviour
{

    bool seMueve = false;
    int rapidez = 10;

    void Update()
    {
        if (transform.position.y >= 8)
        {
            seMueve = true;
        }
        if (transform.position.y <= 0)
        {
            seMueve = false;
        }

        if (seMueve)
        {
            Abajo();
        }
        else
        {
            Arriba();
        }

    }

    void Arriba()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Abajo()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}