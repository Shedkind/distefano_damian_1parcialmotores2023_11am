using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoweUp : MonoBehaviour
{

    public float aumentoVelocidad = 2.0f;
    public float aumentoTamaño = 2.0f;
    public float duracionAumento = 3.0f; 


private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            AumentarJugador(other.GetComponent<Rigidbody>());
            gameObject.SetActive(false);
            
        }
    }

    private void AumentarJugador(Rigidbody jugadorRB)
    {
        jugadorRB.velocity *= aumentoVelocidad;
        Vector3 nuevoTamaño = jugadorRB.transform.localScale * aumentoTamaño;
        jugadorRB.transform.localScale = nuevoTamaño;
    }

   
}
