using UnityEngine;

public class ControlGravedad : MonoBehaviour
{
    public float fuerzaDisparo = 10.0f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            Rigidbody playerRigidbody = other.GetComponent<Rigidbody>();
            Vector3 direccionDisparo = Vector3.up;
            playerRigidbody.AddForce(direccionDisparo * fuerzaDisparo, ForceMode.Impulse);
        }
    }
}