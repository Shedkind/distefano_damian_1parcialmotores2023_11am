using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MovJugador : MonoBehaviour
{
    private Rigidbody rb;
    private int saltosRealizados;
    private int cont;
    private int maxSaltos = 1;
    public int rapidez;
  
    public SphereCollider col;
    public TMP_Text textoCantidadRecolectados;
    public TMP_Text textoGanaste;
    public TMP_Text textoTimer;
    public TMP_Text textoPerdiste;
    public LayerMask Piso;
    public float magnitudSalto;
    private float timer = 90;
    private bool enSuelo;
    private bool estaPegadoALaPared = false;
    private bool juegoTerminado = false;

    void Start()
    {
        rb = GetComponent < Rigidbody >();
        cont = 0;
        textoGanaste.text = "";
        setearTextos();
        saltosRealizados = 0;
    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Cantidad recolectados: " + cont.ToString();
        if (cont >= 5)
        {
            textoGanaste.text = "Ganaste!";
            Time.timeScale = 0;
        }
    }

    private void FixedUpdate()
    {
        if (!juegoTerminado) 
        {
            float movimientoHorizontal = Input.GetAxis("Horizontal");
            float movimientoVertical = Input.GetAxis("Vertical");
            Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);

            rb.AddForce(vectorMovimiento * rapidez);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);

        } else if (other.gameObject.CompareTag("ResetObject"))
        {
            ReiniciarJugador();

        }
    }

    private void Update()   
    {
        if (!juegoTerminado)
        {
            enSuelo = EstaEnPiso();

            if (enSuelo)
            {
                saltosRealizados = 0;
            }

            if (Input.GetKeyDown(KeyCode.Space) && (enSuelo || saltosRealizados < maxSaltos))
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
                saltosRealizados++;
            }

            timer -= Time.deltaTime;

            
            textoTimer.text = "Tiempo: " + Mathf.Max(timer, 0).ToString("F1");

            
            if (timer <= 0)
            {
                juegoTerminado = true;  
                textoPerdiste.text = "�Juego Terminado!";
                Time.timeScale = 0;
            } 

            if (Input.GetKeyDown(KeyCode.R) || transform.position.y < 0 ||transform.position.y > 30)
            {
                ReiniciarJugador();
            }
        }
    }
    
    public void ReiniciarJugador()
    {
        if (!juegoTerminado)
        {
            transform.position = new Vector3(4, 3, 0);
            rb.velocity = Vector3.zero;
        }
    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * 0.9f, Piso);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("ParedInclinada"))
        {
            estaPegadoALaPared = true;
            rb.useGravity = false;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        estaPegadoALaPared = false;
        rb.useGravity = true;
    }
}
