using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoReinicio : MonoBehaviour
{
    private int hp;
    private GameObject Player;
    public int rapidez;
    public int Identificador;   
    public MovJugador Player1;

    void Start()
    {
        hp = 100;
        Player = GameObject.Find("Jugador");
    }

    private void Update()
    {
        transform.LookAt(Player.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void OnTriggerEner(Collider other)
    {
        if (other.gameObject.CompareTag("Player") == true && Identificador == 0)
        {
            
            Player1.transform.position = new Vector3(4, 3, 0);
        }
        
    }
        


}   