using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovCamara : MonoBehaviour
{
    public GameObject Jugador;
    public Vector3 offset;

    void start()
    {
        offset = transform.position - Jugador.transform.position;

    }

    void LateUpdate()
    {

        transform.position = Jugador.transform.position + offset;


    }




}
